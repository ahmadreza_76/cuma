from django.urls import path

from cuma.views import CustomerListAPIView, UpdateRequestView

urlpatterns = [
    path(
        "update/<str:company_name>", UpdateRequestView.as_view(), name="update-request"
    ),
    path("customer", CustomerListAPIView.as_view()),
]
