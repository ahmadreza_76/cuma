import datetime as dt
import logging

from cuma.clients import CompanyServiceClient
from cuma.clients.models import Company
from cuma.models import Customer
from kpn.celery import app

logger = logging.getLogger(__name__)


@app.task(autoretry_for=(Exception,), retry_kwargs={"max_retries": 5, "countdown": 4})
def update_company_data(company_name: str):
    company: Company = CompanyServiceClient.get_company_details(company_name)
    if not company:
        raise Exception("company not found")

    customer, _ = Customer.objects.update_or_create(
        business_name=company.name,
        defaults={
            "legal_form": company.legal_entity.type,
            "date_of_establishment": dt.date(
                year=company.legal_entity.registration_date.year,
                month=company.legal_entity.registration_date.month,
                day=company.legal_entity.registration_date.day,
            ),
            "address": company.establishment[0].address.dict(),
            "chamber_of_commerce": company.kvk_number,
        },
    )

    logger.info(customer)
