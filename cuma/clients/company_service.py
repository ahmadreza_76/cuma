import logging
import typing

import requests
from django.conf import settings

from cuma.clients.models import Company

logger = logging.getLogger(__name__)


class CompanyServiceClient:
    @classmethod
    def get_company_details(cls, company_name: str) -> typing.Optional[Company]:
        url = f"{settings.GET_CAMPAIGN_URL}/{company_name}"
        resp = requests.get(url)
        if not resp.ok:
            logger.error(
                f"failed to fetch data from api endpoint: {url}, {resp.reason}",
                extra={"response": resp.content},
            )
            return None
        company_dict = resp.json().get("response")

        if not company_dict:
            logger.error(
                f"company response empty: {url}, {resp.reason}",
                extra={"response": resp.content},
            )
            return None

        try:
            return Company(**company_dict)
        except TypeError:
            logger.error(
                f"error in parsing company dict: {company_dict}",
                extra={"response": resp.content},
            )
            return None
