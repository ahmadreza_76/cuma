import typing

from pydantic import BaseModel


class Address(BaseModel):
    street_name: str
    house_number: str
    zip_code: str
    city: str
    country: str


class Establishment(BaseModel):
    main: bool
    address: Address


class RegistrationDate(BaseModel):
    year: int
    month: int
    day: int


class LegalEntity(BaseModel):
    type: str
    registration_date: RegistrationDate


class Company(BaseModel):
    kvk_number: str
    name: str
    legal_entity: LegalEntity
    establishment: typing.List[Establishment]
