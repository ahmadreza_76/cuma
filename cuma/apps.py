from django.apps import AppConfig


class CumaConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "cuma"
