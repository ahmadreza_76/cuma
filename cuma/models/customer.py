from django.db import models


class Customer(models.Model):
    business_name = models.CharField(max_length=256, unique=True)
    chamber_of_commerce = models.CharField(max_length=256)
    legal_form = models.CharField(max_length=20)
    address = models.JSONField()
    date_of_establishment = models.DateField()
