from django.http import HttpResponse
from django.views import View

from cuma.tasks import update_company_data


class UpdateRequestView(View):
    def post(self, request, company_name: str):
        update_company_data.apply_async(args=(company_name,))
        return HttpResponse()
