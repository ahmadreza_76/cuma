from rest_framework import generics

from cuma.models import Customer
from cuma.serializers import CustomerSerializer


class CustomerListAPIView(generics.ListAPIView):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
