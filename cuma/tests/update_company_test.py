from unittest import mock

from django.test import override_settings
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from cuma.clients.models import (Address, Company, Establishment, LegalEntity,
                                 RegistrationDate)
from cuma.models import Customer


class UpdateCompanyTestCase(APITestCase):
    # override settings to make celery tasks run synchronous
    @override_settings(
        task_eager_propagates=True,
        task_always_eager=True,
        broker_url="memory://",
        backend="memory",
    )
    def test_update_multiple_company_datas(self):
        response = self.client.post(
            reverse("update-request", kwargs={"company_name": "unilever"})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Customer.objects.count(), 1)

        response = self.client.post(
            reverse("update-request", kwargs={"company_name": "andyscompany"})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Customer.objects.count(), 2)

    # override settings to make celery tasks run synchronous
    @override_settings(
        task_eager_propagates=True,
        task_always_eager=True,
        broker_url="memory://",
        backend="memory",
    )
    @mock.patch("cuma.clients.company_service.CompanyServiceClient.get_company_details")
    def test_update_single_company_twice(self, mock_get_company_details):
        company = Company(
            kvk_number="000001",
            name="magicmicestudios",
            legal_entity=LegalEntity(
                type="BV",
                registration_date=RegistrationDate(year=1990, month=10, day=2),
            ),
            establishment=[
                Establishment(
                    main=True,
                    address=Address(
                        street_name="test",
                        house_number="1",
                        zip_code="1234AB",
                        city="Eindhoven",
                        country="Netherlands",
                    ),
                )
            ],
        )

        mock_get_company_details.return_value = company
        response = self.client.post(
            reverse("update-request", kwargs={"company_name": "magicmicestudios"})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Customer.objects.count(), 1)
        customer = Customer.objects.first()
        self.assertEqual(customer.chamber_of_commerce, "000001")

        company.kvk_number = "000002"

        mock_get_company_details.return_value = company
        response = self.client.post(
            reverse("update-request", kwargs={"company_name": "magicmicestudios"})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Customer.objects.count(), 1)
        customer = Customer.objects.first()
        self.assertEqual(customer.chamber_of_commerce, "000002")
