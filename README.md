# How to Run?
you should have Docker and Docker Compose installed for running the project.

## First create a .env file from .env.example
```bash
cp .env.example .env
```
you can change the setting you want in this .env file

## Run with docker-compose
```bash
docker-compose up --build
```

## Run Tests
```bash
docker exec -it django python manage.py test
```

Now you can access the Django project on port 8000 on your localhost

# How it works?
There is an endpoint for requesting an update in a specific companies data.<br>
example:
```bash
POST /api/update/unilever
```
this endpoint will enqueue a background task in celery for getting the data from MicroService B and update our DB.<br>

We also have an endpoint to expose the customer (in our database) data
```bash
GET /api/customer
```

# What would I do in next steps (if I had more time)?
* Using asyncio in clients and in calls to database.
* Adding more unit tests and integration tests